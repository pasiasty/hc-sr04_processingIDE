import controlP5.*;
import processing.serial.*;
import g4p_controls.*;


ControlP5 cp5;
Serial myPort;
Chart myChart;

String strData;
String strLastData;
String strVeryLastData;
String strRefLevel;
String strDifference;
String strfloatDifference;

int intData;
int intRefLevel = 0;
int minValue = 0;
int lastIntData = 0;
int difference = 0;

float floatLastData = 0;
float floatVeryLastData = 0;
float interpData = 0;
float floatDifference = 0;

boolean start = false;
boolean refLevelSet = false;

Textlabel TextLabel1, TextLabel2, TextLabel3, TextLabel4, TextLabel5;
Button clearStats;

void setup() {
  
  myPort = new Serial(this,"COM6", 9600);
  size(1300, 960);
  smooth();
  frameRate(30);
  smooth();
  
  
  
  cp5 = new ControlP5(this);
  
  cp5.addButton("Start").setValue(0).setPosition(50, 570).setSize(100,50);
  cp5.addButton("Stop").setValue(0).setPosition(160, 570).setSize(100,50);
  cp5.addButton("Set").setValue(0).setPosition(270, 570).setSize(100,50);
  clearStats = cp5.addButton("ClearStats").setValue(0).setPosition(380, 570).setSize(100,50);
  
  TextLabel1 = cp5.addTextlabel("currValue").setText("").setPosition(50,620).setFont(createFont("Georgia",20));
  TextLabel2 = cp5.addTextlabel("set_ref_level").setText("").setPosition(50, 650).setFont(createFont("Georgia",20));
  TextLabel3 = cp5.addTextlabel("maxLevel").setText("").setPosition(50, 680).setFont(createFont("Georgia",20));
  TextLabel4 = cp5.addTextlabel("diffrence").setText("").setPosition(50, 710).setFont(createFont("Georgia",20));
  TextLabel5 = cp5.addTextlabel("click_start").setText("").setPosition(50, 710).setFont(createFont("Georgia",20));
 
  myChart = cp5.addChart("Ultrasonic Scanner")
               .setPosition(50, 50)
               .setSize(1200, 500)
               .setRange(1, 200) //(1,200) 1-200cm
               .setView(Chart.LINE) // use Chart.LINE, Chart.PIE, Chart.AREA, Chart.BAR_CENTERED
               ;

  myChart.getColor().setBackground(color(255, 100));

  myChart.addDataSet("world");
  myChart.setColors("world", color(255));
  myChart.setData("world", new float[100]);
  
  myChart.addDataSet("interp");
  myChart.setColors("interp", color(255), color(0, 255, 0));
  myChart.updateData("interp", new float[100]);

  myChart.setStrokeWeight(1.5);

}


void draw() {
  background(0);
  if(start)
  {
      if (0 < myPort.available()) 
      {
           
           strData = myPort.readStringUntil('\n'); // string representation of value
           strLastData = strData;
           strVeryLastData = strLastData;
      }
      if(strData != null)
      {
           //println(strData);
           
           floatVeryLastData = float(strVeryLastData);
           floatLastData = float(strLastData);           
           interpData = lerp(floatVeryLastData,floatLastData, 0.5);
           
           strData = trim(strData);
           intData = int(strData);
           print(""+interpData+", "+intData);
           
           TextLabel1.setText("Odległość od najbliższej powierzchni [cm]: "+strData);
           if(refLevelSet)
           {
                 TextLabel2.setText("Poziom referencyjny, odległość od najbliższej powierzchni [cm]: "+intRefLevel);
                 
           }
           else
           {   
               TextLabel2.setText("Poziom referencyjny, odległość od najbliższej powierzchni [cm]: brak.");
           }
               floatDifference = (intRefLevel - interpData);
               difference = (intRefLevel - intData);
               
               strDifference = str(difference);
               TextLabel4.setText("Wysokość obiektu [cm]: "+strDifference);
      }
      myChart.addData("world", difference); //intData
    // if the dataSet has more than the required data entries, delete the first one on the left
      myChart.unshift("world", difference); //intData
      if(myChart.getDataSet("world").size()>100) 
      {
           myChart.removeLast("world");
      }
      
      myChart.addData("interp", floatDifference); //intData
    // if the dataSet has more than the required data entries, delete the first one on the left
      myChart.unshift("interp", floatDifference); //intData
      if(myChart.getDataSet("interp").size()>100) 
      {
           myChart.removeLast("interp");
      }
      
  }
}


public void Start()
{
    start = true;
}
public void Stop()
{
  
    start = false;
    myChart.lock();
    myPort.stop();
    
}
public void Set()
{
    refLevelSet = true;
    intRefLevel = intData;
    
}
public void ClearStats()
{
    refLevelSet = false;
    difference = 0;
    minValue = 0;
}